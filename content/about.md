---
title: About
# description: C'est quoi le DevOps?
---

![DevOps](https://foutatech.netlify.app/static/images/devops.png)

## Pourquoi le DevOps est-il important ?
En favorisant la communication et la collaboration entre les équipes chargées du développement et des opérations IT, le DevOps vise à optimiser la satisfaction client et à proposer des solutions à valeur ajoutée plus rapidement. Le DevOps est aussi conçu pour stimuler l'innovation dans une optique d'amélioration continue des processus.

Les pratiques DevOps accélèrent, optimisent et sécurisent la valeur commerciale des entreprises, par exemple via la publication plus fréquente ou la mise à disposition plus rapide de versions, de fonctionnalités ou de mises à jour des produits, le tout en assurant les niveaux de qualité et de sécurité appropriés. Autre objectif : améliorer les délais de détection, de résolution de bogues ou d'autres problèmes et de republication d'une version.

L'infrastructure sous-jacente apporte également au DevOps la fluidité des performances, la disponibilité et la fiabilité requises lors des étapes de développement, de test et de mise en production des logiciels.

## Méthodes DevOps
Afin d'accélérer et d'améliorer le développement et le lancement de leurs produits, les entreprises disposent de plusieurs méthodologies et pratiques DevOps de développement logiciel. Les méthodes Scrum, Kanban et Agile sont les plus couramment utilisées.

**Scrum**. La méthode Scrum définit la manière dont les membres de l'équipe doivent collaborer pour accélérer les projets de développement et d'assurance qualité. Les pratiques Scrum utilisent des workflows clés, une terminologie spécifique (sprint, time box, daily scrum) et des rôles désignés (Scrum Master, product owner ou responsable de produit).  

**Kanban**. Développée par Toyota pour améliorer l'efficacité de ses usines de montage, la méthode Kanban repose sur un suivi des travaux en cours (TEC) dans un projet logiciel à l'aide d'un tableau de Kanban.  

**Agile**. Les premières méthodes de développement logiciel agile continuent d'influencer largement les pratiques et les outils DevOps. De nombreuses approches DevOps, notamment Scrum et Kanban, intègrent des éléments de la programmation agile. Certaines pratiques agiles offrent une meilleure réactivité face à l'évolution des besoins en documentant les exigences sous forme de user stories, en organisant des réunions quotidiennes (daily standups) et en intégrant les retours des clients de manière continue. La méthodologie agile préconise également des cycles de développement logiciel plus courts, à l'encontre des méthodes classiques dites « en cascade » plus chronophages.  


## Chaîne d'outils DevOps
Les adeptes des pratiques DevOps intègrent souvent des outils compatibles DevOps dans leur «chaîne d'outils» pour rationaliser, accélérer et automatiser davantage les différentes étapes du workflow (ou «pipeline») de fourniture des logiciels. Ces outils renforcent les principes fondamentaux du DevOps tels que l'automatisation, la collaboration et l'intégration entre les équipes chargées du développement et des opérations. Voici quelques exemples d'outils employés à différentes étapes du cycle de vie DevOps.

**Planification**. Cette phase permet de définir la valeur commerciale et les exigences. Jira et Git peuvent être utilisés pour le suivi des problèmes connus et la gestion des projets.  

**Code**. Cette phase inclut la conception logicielle et la création du code logiciel à l'aide des logiciels GitHub, GitLab, Bitbucket ou Stash, par exemple.  

**Création**. Cette phase consiste à gérer les versions logicielles et à exploiter des outils automatisés pour compiler et intégrer le code en vue de sa mise en production. Des référentiels de code source ou de package « empaquettent » aussi l'infrastructure requise pour la livraison du produit à l'aide des logiciels Docker, Ansible, Puppet, Chef, Gradle, Maven ou JFrog Artifactory, par exemple.  

**Test**. Cette phase comprend des tests continus, qu'ils soient manuels ou automatisés, et vise à assurer une qualité de code optimale à l'aide des logiciels JUnit, Codeception, Selenium, Vagrant, TestNG ou BlazeMeter, par exemple.  

**Déploiement**. Cette phase peut inclure des outils de gestion, de coordination, de planification et d'automatisation de la mise en production des produits, avec Puppet, Chef, Ansible, Jenkins, Kubernetes, OpenShift, OpenStack, Docker ou Jira, par exemple.  

**Exploitation**. Cette phase permet de gérer les logiciels en production à l'aide des logiciels Ansible, Puppet, PowerShell, Chef, Salt ou Otter, par exemple.  

**Supervision**. Cette phase permet d'identifier les problèmes affectant une version logicielle en production et de collecter les informations correspondantes à l'aide des logiciels New Relic, Datadog, Grafana, Wireshark, Splunk, Nagios ou Slack, par exemple.  

## Pratiques DevOps
Les pratiques DevOps améliorent en continu et automatisent les processus. Bon nombre d'entre elles portent sur une ou plusieurs phases du cycle de développement :  

**Développement continu**. Cette pratique couvre les phases de planification et de codage dans le cycle de vie DevOps et peut inclure des mécanismes de contrôle des versions.  

**Tests continus**. Cette pratique prévoit des tests automatisés, planifiés et continus lors de l'écriture ou de la mise à jour du code de l'application qui accélèrent la livraison du code en production.  

**Intégration continue**. Cette pratique rassemble des outils de gestion de la configuration, de test et de développement pour assurer le suivi de la mise en production des différentes portions du code. Elle implique une collaboration étroite entre les équipes responsables des tests et du développement pour identifier et résoudre rapidement les problèmes de code.  

**Livraison continue**. Cette pratique automatise la publication des modifications du code après la phase de test, dans un environnement intermédiaire ou de préproduction. Un membre de l'équipe peut décider de publier ces modifications dans l'environnement de production.  

**Déploiement continu**. À l'instar de la livraison continue, cette pratique automatise la publication d'un code nouveau ou modifié dans l'environnement de production. Les entreprises peuvent être amenées à publier plusieurs fois par jour des modifications du code ou des fonctionnalités. Dans un contexte de déploiement continu, les technologies de conteneur comme Docker et Kubernetes assurent la cohérence du code entre plusieurs plateformes et environnements.  

**Surveillance continue**. Cette pratique prévoit une surveillance continue du code exécuté et de l'infrastructure sous-jacente. Les développeurs reçoivent des retours sur les bogues ou sur les problèmes.  

**Infrastructure-as-code**. Cette pratique peut être suivie dans plusieurs phases DevOps pour automatiser le provisionnement de l'infrastructure requise pour une version logicielle. Les développeurs ajoutent le « code » de l'infrastructure à l'aide de leurs outils de développement. Par exemple, un développeur peut créer un volume de stockage à la demande via Docker, Kubernetes ou OpenShift. Cette pratique permet aussi aux équipes chargées des opérations de surveiller les configurations de l'environnement, d'effectuer le suivi des modifications et de simplifier leur restauration.  

## Avantages du DevOps
Pour les entreprises qui suivent les pratiques DevOps, les avantages commerciaux et techniques sont évidents et la plupart contribuent à améliorer la satisfaction des clients :

- Accélération et amélioration de la fourniture des produits
- Résolution plus rapide des problèmes et complexité réduite
- Plus grande évolutivité et disponibilité inégalée
- Stabilité accrue des environnements d'exploitation
- Meilleure utilisation des ressources
- Automatisation accrue
- Meilleure visibilité sur les résultats du système
- Innovation renforcée
